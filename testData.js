export const carListData = [
    {
        "id": "0",
        "carId": "254435354081280",
        "upSource": 0,
        "extensionId": "0",
        "upTime": "2024-10-10 18:57:55",
        "organizeId": "0",
        "createTime": null,
        "delState": 0,
        "licensePlate": "川BHZ280(吕建蓉)",
        "terminalNum": "908906",
        "mileage": null,
        "oil": null,
        "acc": 1,
        "locationState": 1,
        "lonGcj": 104.760302,
        "latGcj": 31.480609,
        "direction": 0,
        "speed": 0.0,
        "address": "四川省绵阳市游仙区游仙街道五里堆路恒馨苑",
        "state": 0
    },
    {
        "id": "0",
        "carId": "254434755072000",
        "upSource": 0,
        "extensionId": "0",
        "upTime": "2024-10-16 16:57:15",
        "organizeId": "0",
        "createTime": null,
        "delState": 0,
        "licensePlate": "川B379KV(钟健)",
        "terminalNum": "911738",
        "mileage": null,
        "oil": null,
        "acc": 1,
        "locationState": 1,
        "lonGcj": 105.414076,
        "latGcj": 31.694409,
        "direction": 0,
        "speed": 0.0,
        "address": "四川省广元市剑阁县开封镇小伏路",
        "state": 1
    },
    {
        "id": "0",
        "carId": "254432299493376",
        "upSource": 0,
        "extensionId": "0",
        "upTime": "2024-10-12 17:37:55",
        "organizeId": "0",
        "createTime": null,
        "delState": 0,
        "licensePlate": "川B5YM88(杨鑫)",
        "terminalNum": "908908",
        "mileage": null,
        "oil": null,
        "acc": 1,
        "locationState": 1,
        "lonGcj": 104.763660,
        "latGcj": 31.495877,
        "direction": 0,
        "speed": 0.0,
        "address": "四川省绵阳市游仙区游仙街道九龙集团",
        "state": 0
    },
    {
        "id": "0",
        "carId": "254428557414400",
        "upSource": 0,
        "extensionId": "0",
        "upTime": "2024-10-11 15:52:09",
        "organizeId": "0",
        "createTime": null,
        "delState": 0,
        "licensePlate": "川BGT710(严先平)",
        "terminalNum": "908827",
        "mileage": null,
        "oil": null,
        "acc": 1,
        "locationState": 1,
        "lonGcj": 104.761973,
        "latGcj": 31.494496,
        "direction": 0,
        "speed": 0.0,
        "address": "四川省绵阳市游仙区游仙街道中经路66号九龙能源九龙加油站",
        "state": 0
    },
    {
        "id": "0",
        "carId": "254429314277376",
        "upSource": 0,
        "extensionId": "0",
        "upTime": "2024-10-16 15:12:30",
        "organizeId": "0",
        "createTime": null,
        "delState": 0,
        "licensePlate": "川BGT713(郑志勇)",
        "terminalNum": "908836",
        "mileage": null,
        "oil": null,
        "acc": 1,
        "locationState": 1,
        "lonGcj": 103.956346,
        "latGcj": 30.566440,
        "direction": 0,
        "speed": 0.0,
        "address": "四川省成都市双流区东升街道机场东三路成都双流国际机场",
        "state": 0
    },
    {
        "id": "0",
        "carId": "254434091644928",
        "upSource": 0,
        "extensionId": "0",
        "upTime": "2024-10-16 16:38:20",
        "organizeId": "0",
        "createTime": null,
        "delState": 0,
        "licensePlate": "川BS5839(段明星)",
        "terminalNum": "908849",
        "mileage": null,
        "oil": null,
        "acc": 1,
        "locationState": 1,
        "lonGcj": 108.893767,
        "latGcj": 34.040297,
        "direction": 0,
        "speed": 0.0,
        "address": "陕西省西安市长安区子午街道西安依山客栈西安远山望茶宿",
        "state": 0
    },
    {
        "id": "0",
        "carId": "254432196145152",
        "upSource": 0,
        "extensionId": "0",
        "upTime": "2024-09-17 10:13:57",
        "organizeId": "0",
        "createTime": null,
        "delState": 0,
        "licensePlate": "川B6VC68(向仕强)",
        "terminalNum": "908875",
        "mileage": null,
        "oil": null,
        "acc": 1,
        "locationState": 1,
        "lonGcj": 104.761692,
        "latGcj": 31.494505,
        "direction": 0,
        "speed": 0.0,
        "address": "四川省绵阳市游仙区游仙街道绵山路久利电子公司",
        "state": 0
    },
    {
        "id": "0",
        "carId": "254435827945472",
        "upSource": 0,
        "extensionId": "0",
        "upTime": "2024-10-16 15:17:11",
        "organizeId": "0",
        "createTime": null,
        "delState": 0,
        "licensePlate": "川B3UP89(魏发安)",
        "terminalNum": "908885",
        "mileage": null,
        "oil": null,
        "acc": 1,
        "locationState": 1,
        "lonGcj": 115.480234,
        "latGcj": 39.349502,
        "direction": 0,
        "speed": 0.0,
        "address": "河北省保定市易县易州镇喆·啡酒店(易县燕都古城店)御景·蓝湾2期",
        "state": 0
    },
    {
        "id": "0",
        "carId": "254432458856448",
        "upSource": 0,
        "extensionId": "0",
        "upTime": "2024-10-16 09:59:25",
        "organizeId": "0",
        "createTime": null,
        "delState": 0,
        "licensePlate": "川BZU101(补建军)",
        "terminalNum": "908896",
        "mileage": null,
        "oil": null,
        "acc": 1,
        "locationState": 1,
        "lonGcj": 94.258723,
        "latGcj": 36.369456,
        "direction": 0,
        "speed": 0.0,
        "address": "青海省海西蒙古族藏族自治州格尔木市郭勒木德镇",
        "state": 0
    },
    {
        "id": "0",
        "carId": "254434619160576",
        "upSource": 0,
        "extensionId": "0",
        "upTime": "2024-10-10 17:20:15",
        "organizeId": "0",
        "createTime": null,
        "delState": 0,
        "licensePlate": "川B161KW(尹忠)",
        "terminalNum": "908846",
        "mileage": null,
        "oil": null,
        "acc": 1,
        "locationState": 1,
        "lonGcj": 104.762245,
        "latGcj": 31.495044,
        "direction": 0,
        "speed": 0.0,
        "address": "四川省绵阳市游仙区游仙街道九龙能源九龙加油站",
        "state": 0
    },
    {
        "id": "0",
        "carId": "254430926458880",
        "upSource": 0,
        "extensionId": "0",
        "upTime": "2024-10-11 07:47:10",
        "organizeId": "0",
        "createTime": null,
        "delState": 0,
        "licensePlate": "川BA108M(闫军)",
        "terminalNum": "908844",
        "mileage": null,
        "oil": null,
        "acc": 1,
        "locationState": 1,
        "lonGcj": 106.807791,
        "latGcj": 28.118490,
        "direction": 0,
        "speed": 0.0,
        "address": "贵州省遵义市桐梓县燎原镇城投大厦国裕山园城B区",
        "state": 0
    },
    {
        "id": "0",
        "carId": "254432740960256",
        "upSource": 0,
        "extensionId": "0",
        "upTime": "2024-10-16 15:00:20",
        "organizeId": "0",
        "createTime": null,
        "delState": 0,
        "licensePlate": "川B6WK82(杨阳)",
        "terminalNum": "908853",
        "mileage": null,
        "oil": null,
        "acc": 1,
        "locationState": 1,
        "lonGcj": 104.762270,
        "latGcj": 31.495052,
        "direction": 0,
        "speed": 0.0,
        "address": "四川省绵阳市游仙区游仙街道九龙能源九龙加油站",
        "state": 0
    },
    {
        "id": "0",
        "carId": "254434532190208",
        "upSource": 0,
        "extensionId": "0",
        "upTime": "2024-10-16 13:33:42",
        "organizeId": "0",
        "createTime": null,
        "delState": 0,
        "licensePlate": "川B395KB(贾斌海)",
        "terminalNum": "908848",
        "mileage": null,
        "oil": null,
        "acc": 1,
        "locationState": 1,
        "lonGcj": 106.176190,
        "latGcj": 38.494170,
        "direction": 0,
        "speed": 0.0,
        "address": "宁夏回族自治区银川市金凤区满城北街街道上海西路银川站地面停车场",
        "state": 0
    },
    {
        "id": "0",
        "carId": "254433861447680",
        "upSource": 0,
        "extensionId": "0",
        "upTime": "2024-10-16 11:35:48",
        "organizeId": "0",
        "createTime": null,
        "delState": 0,
        "licensePlate": "川BYP551(段明星)",
        "terminalNum": "908891",
        "mileage": null,
        "oil": null,
        "acc": 1,
        "locationState": 1,
        "lonGcj": 104.763638,
        "latGcj": 31.496157,
        "direction": 0,
        "speed": 0.0,
        "address": "四川省绵阳市游仙区游仙街道九龙集团",
        "state": 0
    },
    {
        "id": "0",
        "carId": "254428095891456",
        "upSource": 0,
        "extensionId": "0",
        "upTime": "2024-09-19 15:53:14",
        "organizeId": "0",
        "createTime": null,
        "delState": 0,
        "licensePlate": "川B037HC(魏威)",
        "terminalNum": "908898",
        "mileage": null,
        "oil": null,
        "acc": 1,
        "locationState": 1,
        "lonGcj": 104.753641,
        "latGcj": 31.514217,
        "direction": 0,
        "speed": 0.0,
        "address": "四川省绵阳市游仙区科学城春雷街道中绵路",
        "state": 0
    },
    {
        "id": "0",
        "carId": "254432906409984",
        "upSource": 0,
        "extensionId": "0",
        "upTime": "2024-10-16 13:37:38",
        "organizeId": "0",
        "createTime": null,
        "delState": 0,
        "licensePlate": "川B923LG(李盛雄)",
        "terminalNum": "908903",
        "mileage": null,
        "oil": null,
        "acc": 1,
        "locationState": 1,
        "lonGcj": 104.763658,
        "latGcj": 31.495874,
        "direction": 0,
        "speed": 0.0,
        "address": "四川省绵阳市游仙区游仙街道九龙集团",
        "state": 0
    },
    {
        "id": "0",
        "carId": "254436453505024",
        "upSource": 0,
        "extensionId": "0",
        "upTime": "2022-02-22 16:43:25",
        "organizeId": "0",
        "createTime": null,
        "delState": 0,
        "licensePlate": "川B73571(栗永涛)",
        "terminalNum": "908825",
        "mileage": null,
        "oil": null,
        "acc": 1,
        "locationState": 1,
        "lonGcj": 104.761694,
        "latGcj": 31.494217,
        "direction": 0,
        "speed": 0.0,
        "address": "四川省绵阳市游仙区游仙街道中经路66号",
        "state": 0
    },
    {
        "id": "0",
        "carId": "254437285830656",
        "upSource": 0,
        "extensionId": "0",
        "upTime": "2023-09-26 16:53:09",
        "organizeId": "0",
        "createTime": null,
        "delState": 0,
        "licensePlate": "川B2HD01(吴涛)",
        "terminalNum": "908900",
        "mileage": null,
        "oil": null,
        "acc": 1,
        "locationState": 1,
        "lonGcj": 104.761987,
        "latGcj": 31.494207,
        "direction": 0,
        "speed": 0.0,
        "address": "四川省绵阳市游仙区游仙街道中经路66号",
        "state": 0
    },
    {
        "id": "0",
        "carId": "254435048560640",
        "upSource": 0,
        "extensionId": "0",
        "upTime": "2024-09-04 12:45:24",
        "organizeId": "0",
        "createTime": null,
        "delState": 0,
        "licensePlate": "川BGR103(王君)",
        "terminalNum": "908859",
        "mileage": null,
        "oil": null,
        "acc": 1,
        "locationState": 1,
        "lonGcj": 105.259895,
        "latGcj": 33.045518,
        "direction": 0,
        "speed": 0.0,
        "address": "甘肃省陇南市武都区洛塘镇洛塘服务区(兰海高速兰州方向)",
        "state": 0
    },
    {
        "id": "0",
        "carId": "254433062696960",
        "upSource": 0,
        "extensionId": "0",
        "upTime": "2024-10-16 15:27:12",
        "organizeId": "0",
        "createTime": null,
        "delState": 0,
        "licensePlate": "川B762JY(吴涛（小）)",
        "terminalNum": "908889",
        "mileage": null,
        "oil": null,
        "acc": 1,
        "locationState": 1,
        "lonGcj": 104.761142,
        "latGcj": 31.493946,
        "direction": 0,
        "speed": 0.0,
        "address": "四川省绵阳市游仙区游仙街道绵山路久利电子公司",
        "state": 0
    },
    {
        "id": "0",
        "carId": "254435919595520",
        "upSource": 0,
        "extensionId": "0",
        "upTime": "2024-09-11 20:24:38",
        "organizeId": "0",
        "createTime": null,
        "delState": 0,
        "licensePlate": "川B7VC33(贾顺)",
        "terminalNum": "908881",
        "mileage": null,
        "oil": null,
        "acc": 1,
        "locationState": 1,
        "lonGcj": 106.255366,
        "latGcj": 32.827343,
        "direction": 0,
        "speed": 0.0,
        "address": "陕西省汉中市宁强县汉源街道羌州南路飞鹿致远酒店",
        "state": 0
    },
    {
        "id": "0",
        "carId": "254428415145984",
        "upSource": 0,
        "extensionId": "0",
        "upTime": "2024-10-10 16:23:43",
        "organizeId": "0",
        "createTime": null,
        "delState": 0,
        "licensePlate": "川B216KZ(王德刚)",
        "terminalNum": "908882",
        "mileage": null,
        "oil": null,
        "acc": 1,
        "locationState": 1,
        "lonGcj": 104.762254,
        "latGcj": 31.495316,
        "direction": 0,
        "speed": 0.0,
        "address": "四川省绵阳市游仙区游仙街道九龙能源九龙加油站",
        "state": 0
    },
    {
        "id": "0",
        "carId": "254434851067904",
        "upSource": 0,
        "extensionId": "0",
        "upTime": "2023-10-03 16:03:47",
        "organizeId": "0",
        "createTime": null,
        "delState": 0,
        "licensePlate": "川BDY800(无)",
        "terminalNum": "908824",
        "mileage": null,
        "oil": null,
        "acc": 1,
        "locationState": 1,
        "lonGcj": 104.768097,
        "latGcj": 31.470882,
        "direction": 0,
        "speed": 0.0,
        "address": "四川省绵阳市游仙区涪江街道一环路东段博大·紫金阁1期",
        "state": 0
    },
    {
        "id": "0",
        "carId": "254433613801472",
        "upSource": 0,
        "extensionId": "0",
        "upTime": "2024-10-16 16:43:00",
        "organizeId": "0",
        "createTime": null,
        "delState": 0,
        "licensePlate": "川BYB551(崔震)",
        "terminalNum": "908867",
        "mileage": null,
        "oil": null,
        "acc": 1,
        "locationState": 1,
        "lonGcj": 108.894033,
        "latGcj": 34.040002,
        "direction": 0,
        "speed": 0.0,
        "address": "陕西省西安市长安区子午街道等风来",
        "state": 0
    },
    {
        "id": "0",
        "carId": "254436270133248",
        "upSource": 0,
        "extensionId": "0",
        "upTime": "2024-10-16 16:58:55",
        "organizeId": "0",
        "createTime": null,
        "delState": 0,
        "licensePlate": "川B73458(敬守帆)",
        "terminalNum": "908905",
        "mileage": null,
        "oil": null,
        "acc": 1,
        "locationState": 1,
        "lonGcj": 104.759744,
        "latGcj": 31.505329,
        "direction": 0,
        "speed": 0.0,
        "address": "四川省绵阳市游仙区游仙街道中国工商银行(九区储蓄所)绿阳居",
        "state": 1
    },
    {
        "id": "0",
        "carId": "254435763245056",
        "upSource": 0,
        "extensionId": "0",
        "upTime": "2024-10-13 15:25:31",
        "organizeId": "0",
        "createTime": null,
        "delState": 0,
        "licensePlate": "川B4PD84(李小平)",
        "terminalNum": "908858",
        "mileage": null,
        "oil": null,
        "acc": 1,
        "locationState": 1,
        "lonGcj": 115.479142,
        "latGcj": 39.342555,
        "direction": 0,
        "speed": 0.0,
        "address": "河北省保定市易县易州镇易兴西路",
        "state": 0
    },
    {
        "id": "0",
        "carId": "254435194576896",
        "upSource": 0,
        "extensionId": "0",
        "upTime": "2024-10-16 16:59:49",
        "organizeId": "0",
        "createTime": null,
        "delState": 0,
        "licensePlate": "川BHZ898(宋良川)",
        "terminalNum": "911741",
        "mileage": null,
        "oil": null,
        "acc": 1,
        "locationState": 1,
        "lonGcj": 104.701866,
        "latGcj": 31.492772,
        "direction": 0,
        "speed": 0.0,
        "address": "四川省绵阳市涪城区创业园街道九洲大道",
        "state": 1
    },
    {
        "id": "0",
        "carId": "254433693489152",
        "upSource": 0,
        "extensionId": "0",
        "upTime": "2024-10-16 16:59:59",
        "organizeId": "0",
        "createTime": null,
        "delState": 0,
        "licensePlate": "川BYD551(张川余)",
        "terminalNum": "908860",
        "mileage": null,
        "oil": null,
        "acc": 1,
        "locationState": 1,
        "lonGcj": 105.694183,
        "latGcj": 38.796330,
        "direction": 0,
        "speed": 0.0,
        "address": "内蒙古自治区阿拉善盟阿拉善左旗巴彦浩特镇G1817乌银高速",
        "state": 1
    },
    {
        "id": "0",
        "carId": "254430361925632",
        "upSource": 0,
        "extensionId": "0",
        "upTime": "2024-10-16 16:58:50",
        "organizeId": "0",
        "createTime": null,
        "delState": 0,
        "licensePlate": "川BHZ281(贾修建)",
        "terminalNum": "169115",
        "mileage": null,
        "oil": null,
        "acc": 1,
        "locationState": 1,
        "lonGcj": 105.411830,
        "latGcj": 31.685527,
        "direction": 0,
        "speed": 0.0,
        "address": "四川省广元市剑阁县开封镇郑家坝",
        "state": 1
    },
    {
        "id": "0",
        "carId": "254435427315712",
        "upSource": 0,
        "extensionId": "0",
        "upTime": "2024-10-16 11:59:50",
        "organizeId": "0",
        "createTime": null,
        "delState": 0,
        "licensePlate": "川BKT507(金伟)",
        "terminalNum": "908854",
        "mileage": null,
        "oil": null,
        "acc": 1,
        "locationState": 1,
        "lonGcj": 107.461308,
        "latGcj": 26.111850,
        "direction": 0,
        "speed": 0.0,
        "address": "贵州省黔南布依族苗族自治州都匀市绿茵湖街道G75兰海高速",
        "state": 0
    },
    {
        "id": "0",
        "carId": "254432376459264",
        "upSource": 0,
        "extensionId": "0",
        "upTime": "2024-07-09 14:16:06",
        "organizeId": "0",
        "createTime": null,
        "delState": 0,
        "licensePlate": "川B0YF35(无)",
        "terminalNum": "908842",
        "mileage": null,
        "oil": null,
        "acc": 1,
        "locationState": 1,
        "lonGcj": 104.740018,
        "latGcj": 31.511703,
        "direction": 0,
        "speed": 0.0,
        "address": "四川省绵阳市游仙区科学城春雷街道科学城七区15栋",
        "state": 0
    },
    {
        "id": "0",
        "carId": "254433220696064",
        "upSource": 0,
        "extensionId": "0",
        "upTime": "2024-10-16 16:38:36",
        "organizeId": "0",
        "createTime": null,
        "delState": 0,
        "licensePlate": "川B593LK(杜金益)",
        "terminalNum": "908855",
        "mileage": null,
        "oil": null,
        "acc": 1,
        "locationState": 1,
        "lonGcj": 104.757257,
        "latGcj": 31.547548,
        "direction": 0,
        "speed": 0.0,
        "address": "四川省绵阳市游仙区游仙街道九院三所新区",
        "state": 0
    },
    {
        "id": "0",
        "carId": "254431055800320",
        "upSource": 0,
        "extensionId": "0",
        "upTime": "2024-10-16 16:22:05",
        "organizeId": "0",
        "createTime": null,
        "delState": 0,
        "licensePlate": "川B572SR(张拯)",
        "terminalNum": "908897",
        "mileage": null,
        "oil": null,
        "acc": 1,
        "locationState": 1,
        "lonGcj": 104.762529,
        "latGcj": 31.495335,
        "direction": 0,
        "speed": 0.0,
        "address": "四川省绵阳市游仙区游仙街道九龙能源九龙加油站",
        "state": 0
    },
    {
        "id": "0",
        "carId": "254432982628352",
        "upSource": 0,
        "extensionId": "0",
        "upTime": "2024-10-13 12:16:22",
        "organizeId": "0",
        "createTime": null,
        "delState": 0,
        "licensePlate": "川B316LD(彭星熙)",
        "terminalNum": "908845",
        "mileage": null,
        "oil": null,
        "acc": 1,
        "locationState": 1,
        "lonGcj": 104.763660,
        "latGcj": 31.495869,
        "direction": 0,
        "speed": 0.0,
        "address": "四川省绵阳市游仙区游仙街道九龙集团",
        "state": 0
    },
    {
        "id": "0",
        "carId": "258959003408384",
        "upSource": 0,
        "extensionId": "0",
        "upTime": "2024-10-16 14:53:02",
        "organizeId": "0",
        "createTime": null,
        "delState": 0,
        "licensePlate": "川B6L937(史潇文)",
        "terminalNum": "908833",
        "mileage": null,
        "oil": null,
        "acc": 1,
        "locationState": 1,
        "lonGcj": 104.751403,
        "latGcj": 31.515892,
        "direction": 0,
        "speed": 0.0,
        "address": "四川省绵阳市游仙区科学城春雷街道中绵路中国工程物理研究院电子工程研究所",
        "state": 0
    },
    {
        "id": "0",
        "carId": "254433922844672",
        "upSource": 0,
        "extensionId": "0",
        "upTime": "2024-09-15 12:06:32",
        "organizeId": "0",
        "createTime": null,
        "delState": 0,
        "licensePlate": "川BYS551(梁智)",
        "terminalNum": "911739",
        "mileage": null,
        "oil": null,
        "acc": 1,
        "locationState": 1,
        "lonGcj": 106.035147,
        "latGcj": 32.697361,
        "direction": 0,
        "speed": 0.0,
        "address": "四川省广元市朝天区中子镇G5京昆高速",
        "state": 0
    },
    {
        "id": "0",
        "carId": "259172779462656",
        "upSource": 0,
        "extensionId": "0",
        "upTime": "2024-10-16 15:15:24",
        "organizeId": "0",
        "createTime": null,
        "delState": 0,
        "licensePlate": "川B3UG90(梁浩)",
        "terminalNum": "908877",
        "mileage": null,
        "oil": null,
        "acc": 1,
        "locationState": 1,
        "lonGcj": 104.763913,
        "latGcj": 31.495869,
        "direction": 0,
        "speed": 0.0,
        "address": "四川省绵阳市游仙区游仙街道仙童街89号九龙集团",
        "state": 0
    },
    {
        "id": "0",
        "carId": "259355832453120",
        "upSource": 0,
        "extensionId": "0",
        "upTime": "2023-11-04 16:20:23",
        "organizeId": "0",
        "createTime": null,
        "delState": 0,
        "licensePlate": "川B42281(汤世强)",
        "terminalNum": "908907",
        "mileage": null,
        "oil": null,
        "acc": 1,
        "locationState": 1,
        "lonGcj": 104.764202,
        "latGcj": 31.497002,
        "direction": 0,
        "speed": 0.0,
        "address": "四川省绵阳市游仙区游仙街道仙童街77绵阳市公安局科学城分局",
        "state": 0
    },
    {
        "id": "0",
        "carId": "260757694521344",
        "upSource": 0,
        "extensionId": "0",
        "upTime": "2024-10-15 17:08:22",
        "organizeId": "0",
        "createTime": null,
        "delState": 0,
        "licensePlate": "川B9P175(苏超)",
        "terminalNum": "908883",
        "mileage": null,
        "oil": null,
        "acc": 1,
        "locationState": 1,
        "lonGcj": 99.720988,
        "latGcj": 40.412037,
        "direction": 0,
        "speed": 0.0,
        "address": "甘肃省酒泉市金塔县航天镇316省道",
        "state": 0
    },
    {
        "id": "0",
        "carId": "259348638656512",
        "upSource": 0,
        "extensionId": "0",
        "upTime": "2024-07-28 14:40:21",
        "organizeId": "0",
        "createTime": null,
        "delState": 0,
        "licensePlate": "川B577MB(向洋)",
        "terminalNum": "911742",
        "mileage": null,
        "oil": null,
        "acc": 1,
        "locationState": 1,
        "lonGcj": 104.790042,
        "latGcj": 31.520576,
        "direction": 0,
        "speed": 0.0,
        "address": "四川省绵阳市游仙区游仙街道绵阳仙海收费站(G93成渝环线高速出口)",
        "state": 0
    },
    {
        "id": "0",
        "carId": "259355252627456",
        "upSource": 0,
        "extensionId": "0",
        "upTime": "2023-04-26 07:26:20",
        "organizeId": "0",
        "createTime": null,
        "delState": 0,
        "licensePlate": "川B75630(齐红九)",
        "terminalNum": "908865",
        "mileage": null,
        "oil": null,
        "acc": 1,
        "locationState": 1,
        "lonGcj": 104.762529,
        "latGcj": 31.495607,
        "direction": 0,
        "speed": 0.0,
        "address": "四川省绵阳市游仙区游仙街道九龙加油站绵阳科学城6区运输部小区",
        "state": 0
    },
    {
        "id": "0",
        "carId": "259359040610304",
        "upSource": 0,
        "extensionId": "0",
        "upTime": "2024-09-29 12:03:19",
        "organizeId": "0",
        "createTime": null,
        "delState": 0,
        "licensePlate": "川B52901(徐陈刚)",
        "terminalNum": "911737",
        "mileage": null,
        "oil": null,
        "acc": 1,
        "locationState": 1,
        "lonGcj": 104.701305,
        "latGcj": 31.479730,
        "direction": 0,
        "speed": 0.0,
        "address": "四川省绵阳市涪城区创业园街道东方幼儿园",
        "state": 0
    },
    {
        "id": "0",
        "carId": "254435490416640",
        "upSource": 0,
        "extensionId": "0",
        "upTime": "2024-10-16 14:32:36",
        "organizeId": "0",
        "createTime": null,
        "delState": 0,
        "licensePlate": "川BKT936(曹洋)",
        "terminalNum": "908879",
        "mileage": null,
        "oil": null,
        "acc": 1,
        "locationState": 1,
        "lonGcj": 104.762251,
        "latGcj": 31.495313,
        "direction": 0,
        "speed": 0.0,
        "address": "四川省绵阳市游仙区游仙街道九龙能源九龙加油站",
        "state": 0
    },
    {
        "id": "0",
        "carId": "259344054718464",
        "upSource": 0,
        "extensionId": "0",
        "upTime": "2024-10-16 17:00:02",
        "organizeId": "0",
        "createTime": null,
        "delState": 0,
        "licensePlate": "川B0PU97(苏伟)",
        "terminalNum": "908839",
        "mileage": null,
        "oil": null,
        "acc": 1,
        "locationState": 1,
        "lonGcj": 106.862478,
        "latGcj": 33.052629,
        "direction": 0,
        "speed": 0.0,
        "address": "陕西省汉中市南郑区阳春镇G5京昆高速",
        "state": 1
    },
    {
        "id": "0",
        "carId": "259348757721088",
        "upSource": 0,
        "extensionId": "0",
        "upTime": "2024-10-09 17:06:49",
        "organizeId": "0",
        "createTime": null,
        "delState": 0,
        "licensePlate": "川B183MV(林冬青)",
        "terminalNum": "908834",
        "mileage": null,
        "oil": null,
        "acc": 1,
        "locationState": 1,
        "lonGcj": 104.761984,
        "latGcj": 31.493949,
        "direction": 0,
        "speed": 0.0,
        "address": "四川省绵阳市游仙区游仙街道中经路62号",
        "state": 0
    },
    {
        "id": "0",
        "carId": "259343949133824",
        "upSource": 0,
        "extensionId": "0",
        "upTime": "2024-10-03 08:53:21",
        "organizeId": "0",
        "createTime": null,
        "delState": 0,
        "licensePlate": "川B635BV(谭武汉)",
        "terminalNum": "908830",
        "mileage": null,
        "oil": null,
        "acc": 1,
        "locationState": 1,
        "lonGcj": 104.758082,
        "latGcj": 31.511436,
        "direction": 0,
        "speed": 0.0,
        "address": "四川省绵阳市游仙区科学城春雷街道绵山路科学城一区",
        "state": 0
    },
    {
        "id": "0",
        "carId": "259348898398208",
        "upSource": 0,
        "extensionId": "0",
        "upTime": "2024-09-25 11:13:42",
        "organizeId": "0",
        "createTime": null,
        "delState": 0,
        "licensePlate": "川BGR105(姜增勇)",
        "terminalNum": "908888",
        "mileage": null,
        "oil": null,
        "acc": 1,
        "locationState": 1,
        "lonGcj": 105.873962,
        "latGcj": 32.609574,
        "direction": 0,
        "speed": 0.0,
        "address": "四川省广元市朝天区朝天镇G5京昆高速明月峡景区",
        "state": 0
    },
    {
        "id": "0",
        "carId": "260955133640704",
        "upSource": 0,
        "extensionId": "0",
        "upTime": "2024-10-16 14:36:09",
        "organizeId": "0",
        "createTime": null,
        "delState": 0,
        "licensePlate": "川B9ZA53(朱正炜)",
        "terminalNum": "908828",
        "mileage": null,
        "oil": null,
        "acc": 1,
        "locationState": 1,
        "lonGcj": 104.761707,
        "latGcj": 31.494763,
        "direction": 0,
        "speed": 0.0,
        "address": "四川省绵阳市游仙区游仙街道绵山路九龙能源九龙加油站",
        "state": 0
    },
    {
        "id": "0",
        "carId": "259343177359360",
        "upSource": 0,
        "extensionId": "0",
        "upTime": "2024-10-16 13:57:57",
        "organizeId": "0",
        "createTime": null,
        "delState": 0,
        "licensePlate": "川B2M905(顾红)",
        "terminalNum": "908886",
        "mileage": null,
        "oil": null,
        "acc": 1,
        "locationState": 1,
        "lonGcj": 104.756983,
        "latGcj": 31.502001,
        "direction": 0,
        "speed": 0.0,
        "address": "四川省绵阳市游仙区科学城春雷街道中绵路四川省科学城医院",
        "state": 0
    },
    {
        "id": "0",
        "carId": "254428264155136",
        "upSource": 0,
        "extensionId": "0",
        "upTime": "2023-10-04 11:11:19",
        "organizeId": "0",
        "createTime": null,
        "delState": 0,
        "licensePlate": "川B029NN(王伟2)",
        "terminalNum": "908832",
        "mileage": null,
        "oil": null,
        "acc": 1,
        "locationState": 1,
        "lonGcj": 104.764199,
        "latGcj": 31.496441,
        "direction": 0,
        "speed": 0.0,
        "address": "四川省绵阳市游仙区游仙街道绵阳市公安局科学城分局",
        "state": 0
    },
    {
        "id": "0",
        "carId": "259356291276800",
        "upSource": 0,
        "extensionId": "0",
        "upTime": "2023-04-24 15:08:53",
        "organizeId": "0",
        "createTime": null,
        "delState": 0,
        "licensePlate": "川B78373(许进强)",
        "terminalNum": "908862",
        "mileage": null,
        "oil": null,
        "acc": 1,
        "locationState": 1,
        "lonGcj": 104.748065,
        "latGcj": 31.508380,
        "direction": 0,
        "speed": 0.0,
        "address": "四川省绵阳市游仙区科学城春雷街道54路餐厅科学城3区",
        "state": 0
    },
    {
        "id": "0",
        "carId": "259348520857600",
        "upSource": 0,
        "extensionId": "0",
        "upTime": "2024-09-18 07:58:18",
        "organizeId": "0",
        "createTime": null,
        "delState": 0,
        "licensePlate": "川B269KZ(刘涛)",
        "terminalNum": "908870",
        "mileage": null,
        "oil": null,
        "acc": 1,
        "locationState": 1,
        "lonGcj": 104.750847,
        "latGcj": 31.515048,
        "direction": 0,
        "speed": 0.0,
        "address": "四川省绵阳市游仙区科学城春雷街道中绵路科学城公园",
        "state": 0
    },
    {
        "id": "0",
        "carId": "259349201932288",
        "upSource": 0,
        "extensionId": "0",
        "upTime": "2024-09-27 18:17:44",
        "organizeId": "0",
        "createTime": null,
        "delState": 0,
        "licensePlate": "川B863TJ(彭舒平)",
        "terminalNum": "908887",
        "mileage": null,
        "oil": null,
        "acc": 1,
        "locationState": 1,
        "lonGcj": 104.763655,
        "latGcj": 31.495882,
        "direction": 0,
        "speed": 0.0,
        "address": "四川省绵阳市游仙区游仙街道九龙集团",
        "state": 0
    },
    {
        "id": "0",
        "carId": "259352942718976",
        "upSource": 0,
        "extensionId": "0",
        "upTime": "2024-10-16 16:58:38",
        "organizeId": "0",
        "createTime": null,
        "delState": 0,
        "licensePlate": "川B44278(戴世云)",
        "terminalNum": "908893",
        "mileage": null,
        "oil": null,
        "acc": 1,
        "locationState": 1,
        "lonGcj": 105.203672,
        "latGcj": 31.639285,
        "direction": 0,
        "speed": 0.0,
        "address": "四川省绵阳市梓潼县文昌镇108国道",
        "state": 1
    },
    {
        "id": "0",
        "carId": "334684228083712",
        "upSource": 0,
        "extensionId": "0",
        "upTime": "2024-09-16 09:12:55",
        "organizeId": "0",
        "createTime": null,
        "delState": 0,
        "licensePlate": "川B82CV5(赵元正)",
        "terminalNum": "908876",
        "mileage": null,
        "oil": null,
        "acc": 1,
        "locationState": 1,
        "lonGcj": 108.288299,
        "latGcj": 34.244132,
        "direction": 0,
        "speed": 0.0,
        "address": "陕西省咸阳市武功县小村镇G30连霍高速武功服务区(连霍高速连云港方向)",
        "state": 0
    },
    {
        "id": "0",
        "carId": "258830325981184",
        "upSource": 0,
        "extensionId": "0",
        "upTime": "2024-10-16 16:56:03",
        "organizeId": "0",
        "createTime": null,
        "delState": 0,
        "licensePlate": "川BU3529(李港)",
        "terminalNum": "908863",
        "mileage": null,
        "oil": null,
        "acc": 1,
        "locationState": 1,
        "lonGcj": 105.205640,
        "latGcj": 31.909511,
        "direction": 0,
        "speed": 0.0,
        "address": "四川省广元市剑阁县东宝镇老君庙",
        "state": 1
    },
    {
        "id": "0",
        "carId": "355952628920320",
        "upSource": 0,
        "extensionId": "0",
        "upTime": "2024-10-16 16:59:41",
        "organizeId": "0",
        "createTime": null,
        "delState": 0,
        "licensePlate": "川B5K739(曾杰)",
        "terminalNum": "908884",
        "mileage": null,
        "oil": null,
        "acc": 1,
        "locationState": 1,
        "lonGcj": 104.827800,
        "latGcj": 32.112104,
        "direction": 0,
        "speed": 0.0,
        "address": "四川省绵阳市平武县响岩镇女寺坝",
        "state": 1
    },
    {
        "id": "0",
        "carId": "370637748301824",
        "upSource": 0,
        "extensionId": "0",
        "upTime": "2024-10-16 16:58:42",
        "organizeId": "0",
        "createTime": null,
        "delState": 0,
        "licensePlate": "川B70FK2(蒲兴勇)",
        "terminalNum": "908890",
        "mileage": null,
        "oil": null,
        "acc": 1,
        "locationState": 1,
        "lonGcj": 105.685261,
        "latGcj": 34.520543,
        "direction": 0,
        "speed": 0.0,
        "address": "甘肃省天水市秦州区皂郊镇G30连霍高速",
        "state": 1
    },
    {
        "id": "0",
        "carId": "370828686956544",
        "upSource": 0,
        "extensionId": "0",
        "upTime": "2024-10-14 10:26:10",
        "organizeId": "0",
        "createTime": null,
        "delState": 0,
        "licensePlate": "川B72FK3(王小平)",
        "terminalNum": "908873",
        "mileage": null,
        "oil": null,
        "acc": 1,
        "locationState": 1,
        "lonGcj": 99.724597,
        "latGcj": 40.420915,
        "direction": 0,
        "speed": 0.0,
        "address": "甘肃省酒泉市金塔县航天镇陈生达口腔诊所河东里市场",
        "state": 0
    },
    {
        "id": "0",
        "carId": "370827136788480",
        "upSource": 0,
        "extensionId": "0",
        "upTime": "2024-10-16 16:56:55",
        "organizeId": "0",
        "createTime": null,
        "delState": 0,
        "licensePlate": "川B20KK9(敬渝怀)",
        "terminalNum": "908872",
        "mileage": null,
        "oil": null,
        "acc": 1,
        "locationState": 1,
        "lonGcj": 105.117356,
        "latGcj": 31.562738,
        "direction": 0,
        "speed": 0.0,
        "address": "四川省绵阳市梓潼县石牛镇绵苍巴高速",
        "state": 1
    },
    {
        "id": "0",
        "carId": "370828934193152",
        "upSource": 0,
        "extensionId": "0",
        "upTime": "2024-10-16 14:45:48",
        "organizeId": "0",
        "createTime": null,
        "delState": 0,
        "licensePlate": "川B83GG2(胡诚)",
        "terminalNum": "908910",
        "mileage": null,
        "oil": null,
        "acc": 1,
        "locationState": 1,
        "lonGcj": 104.761689,
        "latGcj": 31.494491,
        "direction": 0,
        "speed": 0.0,
        "address": "四川省绵阳市游仙区游仙街道绵山路久利电子公司",
        "state": 0
    },
    {
        "id": "0",
        "carId": "408346469236736",
        "upSource": 0,
        "extensionId": "0",
        "upTime": "2024-10-16 13:37:59",
        "organizeId": "0",
        "createTime": null,
        "delState": 0,
        "licensePlate": "川B76JV2(程嘉宇   )",
        "terminalNum": "908911",
        "mileage": null,
        "oil": null,
        "acc": 1,
        "locationState": 1,
        "lonGcj": 104.761150,
        "latGcj": 31.494217,
        "direction": 0,
        "speed": 0.0,
        "address": "四川省绵阳市游仙区游仙街道绵山路久利电子公司",
        "state": 0
    },
    {
        "id": "0",
        "carId": "370828216236032",
        "upSource": 0,
        "extensionId": "0",
        "upTime": "2024-10-16 16:20:06",
        "organizeId": "0",
        "createTime": null,
        "delState": 0,
        "licensePlate": "川B05DK2(初建刚)",
        "terminalNum": "908878",
        "mileage": null,
        "oil": null,
        "acc": 1,
        "locationState": 1,
        "lonGcj": 104.758659,
        "latGcj": 31.546445,
        "direction": 0,
        "speed": 0.0,
        "address": "四川省绵阳市游仙区游仙街道九院三所新区",
        "state": 0
    }
]