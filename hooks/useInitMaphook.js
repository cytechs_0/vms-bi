import {onMounted, ref} from "vue";
import AMapLoader from "@amap/amap-jsapi-loader";
import {carListData} from "../testData.js"

export const useInitMapHook = (id)=>{
    let map = ref(null)
    let AMap1 = ref(null)


    // 添加地图聚合点
    const addTextMarker = (list)=>{
        list.forEach(item=>{
            // 创建点标记并设置文本标签
            let marker = new AMap.Marker({
                position: [item.lonGcj,item.latGcj],
                // icon: '//a.amap.com/jsapi_demos/static/demo-center/icons/poi-marker-default.png',
                offset: new AMap.Pixel(-13, -30)
            });
            marker.setLabel({
                offset: new AMap.Pixel(10, 4),  //设置文本标注偏移量
                content: `<div class='info' style="color:#0f1325">${item.licensePlate}</div>`, //设置文本标注内容
                direction: 'right' //设置文本标注方位
            });
            marker.setMap(map);
        })
    }


    // 添加聚合点样式
    const addClusterMarker = (list,AMap,map)=>{
        let points = [];
        let styles = [
            {
                url: "//a.amap.com/jsapi_demos/static/images/blue.png", //图标显示图片的地址
                size: new AMap.Size(32, 32), //图标显示图片的大小
                offset: new AMap.Pixel(-16, -16), //图标定位在地图上的位置相对于图标左上角的偏移值
            },
            // 更多样式...
        ];
        list.forEach(item=>{
            points.push({
                lnglat:[item.lonGcj,item.latGcj],
                name:item.licensePlate
            })
        })

        let _renderClusterMarker = function (context) {
            let clusterCount = context.count; //聚合点内点数量

            context.marker.setContent(
                '<div class="clusterMarker" >' + clusterCount + "</div>"
            );
            // 自定义点击事件
            context.marker.on('click', function(e) {
                console.log(e)
                var curZoom = map.getZoom();
                if(curZoom < 20){
                    curZoom += 2;
                }
                map.setZoomAndCenter(curZoom, e.lnglat);
            });
        };

        let _renderMarker = function (context) {
            console.log(context,"223232323")

            context.marker.setContent(`<div class="textMarker">${context.data[0].name}</div>`);
        };
        return {
            points,
            styles,
            _renderClusterMarker,
            _renderMarker
        }
    }


    /*
    * 初始化地图
    * */
    const initMap =async ()=>{
       await AMapLoader.load({
            key: "3919f0b2f9b3f928d7e3f348d4d5e4f2", //申请好的Web端开发者 Key，调用 load 时必填
            version: "2.0", //指定要加载的 JS API 的版本，缺省时默认为 1.4.15

        }).then((AMap) => {
            map = new AMap.Map(id,{
                zoom: 6, // 设置地图的缩放级别
                viewMode: "2D",
                // center:['106.5478767','29.5647398']
            });
            map.setMapStyle("amap://styles/darkblue"); // 修改地图的显示样式为暗色主题
            AMap1 = AMap

           // addTextMarker(carListData)

           // 添加聚合点
           let {points,styles,_renderMarker,_renderClusterMarker} = addClusterMarker(carListData,AMap,map);
           map.plugin(["AMap.MarkerCluster"], function () {
               let cluster = new AMap.MarkerCluster(
                   map, //地图实例
                   points, //海量点数据，数据中需包含经纬度信息字段 lnglat
                   {
                       gridSize: 60, //数据聚合计算时网格的像素大小
                       renderClusterMarker: _renderClusterMarker, //自定义聚合点样式
                       renderMarker: _renderMarker, //自定义非聚合点样式

                   }
               );
           });


        })
            .catch((e) => {
                console.error(e); //加载错误提示
            });
    }
    onMounted(async ()=>{
       await initMap()
    })

    return {map,AMap:AMap1};
}