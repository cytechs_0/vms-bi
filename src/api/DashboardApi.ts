import {BasicApi} from "@/api/basicApi/BasicApi";

class DashboardApi extends BasicApi{
    constructor(prefix: string) {
        super(prefix);
    }


    /*
    * 获取实时排放浓度
    * 获取实时排放量
    * */
    getRealTimeValues(){
        return this.request.get("carbon/getRealTimeValues")
    }

    /*
    * 获取24小时排放量
    * 小时累计排放量
    * */
    getSumValues(minNum:number,field:string){
        return this.request.get("carbon/getSumValues",{
            params:{
                minNum,
                field
            }
        })
    }


    /*
    * 获取历史数据
    * */
    getHistoryData(startTime:string,endTime:string,deviceNos?:string){
        return this.request.get("carbon/getHistoryData",{
            params:{
                startTime,
                endTime,
                deviceNos
            }
        })
    }

}

export const dashboardApi = new DashboardApi("carbon");


