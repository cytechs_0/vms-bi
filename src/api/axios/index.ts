import axios from 'axios';
// 配置新建一个 axios 实例
const service = axios.create({
    baseURL: "/api/bi",
    timeout: 1000*100,
    headers: { 'Content-Type': 'application/json' },
});

// 添加请求拦截器
service.interceptors.request.use(
    (config) => {
        // 在发送请求之前做些什么 token
        // if (Session.get('token')) {
        //     config.headers.token = Session.get('token');
        // }
        return config;
    },
    (error) => {
        // 对请求错误做些什么
        return Promise.reject(error);
    }
);

// 添加响应拦截器
service.interceptors.response.use(
    (response) => {
        // 对响应数据做点什么

        return response.data

    },
    (error) => {

        return Promise.reject(error);
    }
);

// 导出 axios 实例
export default service;
