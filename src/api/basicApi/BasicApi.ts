
import request from "@/api/axios/index";
import {AxiosInstance} from "axios";
// @ts-ignore
import qs from "qs";




class BasicApi {
    request:AxiosInstance = request
    prefix:string =""
    constructor(prefix:string="") {
        this.prefix = prefix;
    }

    // 新增
    add(data:object,sectionId?:number |null ):Promise<any>{
        if (sectionId){
            return this.request.post(`${this.prefix}/add`,data,{
                headers:{
                    sectionId,
                }
            })
        }else{
            return this.request.post(`${this.prefix}/add`,data,)
        }

    }
    // 编辑
    updateById(data:object):Promise<any>{
        return this.request.post(`${this.prefix}/updateById`,data)
    }

    // 删除byId
    removeById(data:object):Promise<any>{
        return this.request.post(`${this.prefix}/removeById`,data)
    }
    // 查询所有
    getAll(data:object):Promise<any>{
        return this.request.post(`${this.prefix}/getAll`,data)
    }
    // 分页查询
    getPage(data:object):Promise<any>{
        return this.request.post(`${this.prefix}/getPage`,data)
    }
    // 查询单个
    getById(data:object):Promise<any>{
        return this.request.post(`${this.prefix}/getById`,data)
    }

    // 删除byIds
    // ids 使用qs.stringify({ ids: ids }, { arrayFormat: 'brackets' })
    removeByIds(ids:object,sectionId?:number | null):Promise<any>{
        let params = qs.stringify({ ids: ids }, { arrayFormat: 'brackets' })
        if (sectionId){
            return this.request.post(`${this.prefix}/removeByIds?${params}`,{},{
                headers:{
                    sectionId:sectionId
                }
            })
        }else{
            return this.request.post(`${this.prefix}/removeByIds?${params}`)
        }

    }

}

export {BasicApi}
