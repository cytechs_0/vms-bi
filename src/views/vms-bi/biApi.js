import {BasicApi} from "@/api/basicApi/BasicApi";

class BiApi extends BasicApi{
    constructor(prefix) {
        super(prefix);
    }

    /*
    * 1、获取车辆信息
    * */
    getInformation(){
        return this.request.get("/vehicle/information")
    }


    /*
    *
    * 2、报警信息
    * */



    /*
    *
    * 里程前十
    * */
    getMileageTop5(){
        return this.request.get("/car/mileage/top5")
    }



    /*
    *
    * 部门前五
    * */
    getDepartmentTop5(){
        return this.request.get("/top5/department")
    }




    /*
    *
    * 出车数量
    * */
    getCountMonth(){
        return this.request.get("/order/count/month")
    }


    /*
    *
    * 出车里程
    * */
    getMileageMonth(){
        return this.request.get("/mileage/count/month")
    }

    /*
    * 监控
    * */
    get_real_time_monitoring(){
        return this.request.get("/real_time_monitoring")
    }

}

export const biApi = new BiApi("bi");


