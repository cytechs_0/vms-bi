




// 地图添加标记
export const addTextMarker = (list,AMap,map)=>{
    list.forEach(item=>{
        // 创建点标记并设置文本标签
        let marker = new AMap.Marker({
            position: [item.lonGcj,item.latGcj],
            // icon: '//a.amap.com/jsapi_demos/static/demo-center/icons/poi-marker-default.png',
            offset: new AMap.Pixel(-13, -30)
        });
        marker.setLabel({
            offset: new AMap.Pixel(10, 4),  //设置文本标注偏移量
            content: `<div class='info' style="color:#0f1325">${item.licensePlate}</div>`, //设置文本标注内容
            direction: 'right' //设置文本标注方位
        });
        marker.setMap(map);
    })
}

// 地图添加聚合点
export const addClusterMarker = (list,AMap,map)=>{
    let points = [];
    let styles = [
        {
            url: "//a.amap.com/jsapi_demos/static/images/blue.png", //图标显示图片的地址
            size: new AMap.Size(32, 32), //图标显示图片的大小
            offset: new AMap.Pixel(-16, -16), //图标定位在地图上的位置相对于图标左上角的偏移值
        },
        // 更多样式...
    ];
    list.forEach(item=>{
        points.push({
            lnglat:[item.lonGcj,item.latGcj],
            name:item.licensePlate,
            status:item.status
        })
    })

    let _renderClusterMarker = function (context) {
        let clusterCount = context.count; //聚合点内点数量

        context.marker.setContent(
            '<div class="clusterMarker" >' + clusterCount + "</div>"
        );
        // 自定义点击事件
        context.marker.on('click', function(e) {
            console.log(e)
            var curZoom = map.getZoom();
            if(curZoom < 20){
                curZoom += 2;
            }
            map.setZoomAndCenter(curZoom, e.lnglat);
        });
    };

    let _renderMarker = function (context) {
        context.marker.setContent(`<div class="textMarker">${context.data[0].name}</div>`);
    };
    return {
        points,
        styles,
        _renderClusterMarker,
        _renderMarker
    }
}