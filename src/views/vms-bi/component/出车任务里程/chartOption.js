import * as echarts from "echarts";
export const chartOptionFun = (xData,yData)=>{
    return   {
        title: {
            text: '出车任务里程',
            textStyle: {
                align: 'center',
                color: '#fff',
                fontSize: 16,
            },
            top: '8',
            left: 'center',
        },
        // backgroundColor: '#0f375f',
        grid: {
            top: "15%",
            bottom: "10%",//也可设置left和right设置距离来控制图表的大小
            left: "15%"
        },
        tooltip: {
            trigger: "axis",
            axisPointer: {
                type: "shadow",
                label: {
                    // show: true
                }
            },

        },
        xAxis: {
            data: xData,
            axisLine: {
                show: false, //隐藏X轴轴线
                lineStyle: {
                    color: '#01FCE3'
                }
            },
            axisTick: {
                show: false //隐藏X轴刻度
            },
            axisLabel: {
                show: true,
                color: "#fff" //X轴文字颜色
            },

        },
        yAxis: [{
            type: "value",
            // name: "ceshi",
            nameTextStyle: {
                color: "#ebf8ac"
            },
            splitLine: {
                show: false
            },
            axisTick: {
                show: false
            },
            axisLine: {
                show: false,
                lineStyle: {
                    color: '#FFFFFF'
                }
            },
            axisLabel: {
                show: true,
                color: "#fff"
            },

        },
            {
                type: "value",
                name: "同比",
                show: false,
                nameTextStyle: {
                    color: "#ebf8ac"
                },
                position: "right",
                splitLine: {
                    show: false
                },
                axisTick: {
                    show: false
                },
                axisLine: {
                    show: false
                },
                axisLabel: {
                    show: true,
                    formatter: "{value} %", //右侧Y轴文字显示
                    color: ""
                }
            }
        ],
        series: [{
            name: "任务里程（km）",
            type: "line",
            yAxisIndex: 1, //使用的 y 轴的 index，在单个图表实例中存在多个 y轴的时候有用
            smooth: true, //平滑曲线显示
            showAllSymbol: true, //显示所有图形。
            symbol: "circle", //标记的图形为实心圆
            symbolSize: 8, //标记的大小
            itemStyle: {
                //折线拐点标志的样式
                // 径向渐变，前三个参数分别是圆心 x, y 和半径，取值同线性渐变
                color: {
                    type: 'radial',
                    x: 0.5,
                    y: 0.5,
                    r: 0.5,
                    colorStops: [
                        {
                            offset: 0, color: '#fff' // 0% 处的颜色
                        }, {
                            offset: 0.25, color: '#fff' // 100% 处的颜色
                        }, {
                            offset: 0.5, color: '#058cff' // 100% 处的颜色
                        },  {
                            offset: 1, color: 'red' // 100% 处的颜色
                        }
                    ],
                    global: false, // 缺省为 false
                    emphasis:{
                        color: '#EB547C',
                        borderColor: '#EB547C',
                        borderWidth: 40,
                    }
                }
            },
            lineStyle: {
                color: "#058cff"
            },
            areaStyle:{
                color: "rgba(5,140,255, 0)"
            },
            data: yData
        },
            {
                // name: "主营业务",
                type: "bar",
                barWidth: 1,
                tooltip: {
                    show: false
                },
                itemStyle: {
                    color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                        offset: 0,
                        color: "#1268f3"
                    },
                        // {
                        //     offset: 0.5,
                        //     color: "#4693EC"
                        // },
                        {
                            offset: 1,
                            color: "rgba(250, 250, 250, 0)"
                        }
                    ])
                },
                data: yData
            }
        ]
    }

}