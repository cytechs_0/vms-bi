import * as echarts from "echarts";
export const chartOptionFun = (xData,yData)=>{
    return  {
        // backgroundColor: "#003366",
        title: {
            text: "出车任务数量",
            textStyle: {
                align: "center",
                color: "#fff",
                fontSize: 16,
            },
            top: "8",
            left: "center",
        },
        tooltip: {
            trigger: "axis",

            axisPointer: {
                lineStyle: {
                    color: {
                        type: "linear",
                        x: 0,
                        y: 0,
                        x2: 0,
                        y2: 1,
                        colorStops: [
                            {
                                offset: 0,
                                color: "rgba(0, 255, 233,0)",
                            },
                            {
                                offset: 0.5,
                                color: "rgba(255, 255, 255,1)",
                            },
                            {
                                offset: 1,
                                color: "rgba(0, 255, 233,0)",
                            },
                        ],
                        global: false,
                    },
                },
            },
        },
        grid: {
            top: "15%",
            left: "5%",
            right: "5%",
            bottom: "15%",
            // containLabel: true
        },
        xAxis: [
            {
                type: "category",
                axisLine: {
                    show: true,
                },
                splitArea: {
                    // show: true,
                    color: "#f00",
                    lineStyle: {
                        color: "#f00",
                    },
                },
                axisLabel: {
                    color: "#fff",
                },
                splitLine: {
                    show: false,
                },
                boundaryGap: false,
                data: xData,
            },
        ],

        yAxis: [
            {
                type: "value",
                min: 0,
                // max: 140,
                splitNumber: 4,
                splitLine: {
                    show: true,
                    lineStyle: {
                        color: "rgba(255,255,255,0.1)",
                    },
                },
                axisLine: {
                    show: false,
                },
                axisLabel: {
                    show: false,
                    margin: 20,
                    textStyle: {
                        color: "#d1e6eb",
                    },
                },
                axisTick: {
                    show: false,
                },
            },
        ],
        series: [
            {
                name: "任务数量",
                type: "line",
                // smooth: true, //是否平滑
                showAllSymbol: true,
                // symbol: 'image://./static/images/guang-circle.png',
                symbol: "circle",
                symbolSize: 12,
                lineStyle: {
                    normal: {
                        color: "#6c50f3",
                        shadowColor: "rgba(0, 0, 0, .3)",
                        shadowBlur: 0,
                        shadowOffsetY: 5,
                        shadowOffsetX: 5,
                    },
                },
                label: {
                    show: true,
                    position: "top",
                    textStyle: {
                        color: "#6c50f3",
                    },
                },
                itemStyle: {
                    color: "#6c50f3",
                    borderColor: "#fff",
                    borderWidth: 3,
                    shadowColor: "rgba(0, 0, 0, .3)",
                    shadowBlur: 0,
                    shadowOffsetY: 2,
                    shadowOffsetX: 2,
                },
                tooltip: {
                    show: true,
                },
                areaStyle: {
                    normal: {
                        color: new echarts.graphic.LinearGradient(
                            0,
                            0,
                            0,
                            1,
                            [
                                {
                                    offset: 0,
                                    color: "rgba(108,80,243,0.3)",
                                },
                                {
                                    offset: 1,
                                    color: "rgba(108,80,243,0)",
                                },
                            ],
                            false
                        ),
                        shadowColor: "rgba(108,80,243, 0.9)",
                        shadowBlur: 20,
                    },
                },
                data: yData,
            },
        ],
    };

}