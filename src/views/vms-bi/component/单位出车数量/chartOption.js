import * as echarts from "echarts";
export const chartOptionFun = (data=[])=>{
    return   {
        // backgroundColor: "#003366",
        title: {
            text: "单位出车数量",
            left: "center",
            top: 0,
            textStyle: {
                fontWeight: "normal",
                color: "#FFF",
                fontSize: 16,
            },
        },

        tooltip: {
            trigger: "item",
            formatter: "{b} : {c} ({d}%)",
        },

        visualMap: {
            show: false,
            min: 500,
            max: 600,
            inRange: {
                // colorLightness: [0, 1]
            },
        },
        series: [
            {
                name: "访问来源",
                type: "pie",
                radius: "70%",
                center: ["50%", "60%"],
                color: ["rgb(131,249,103)", "#FBFE27", "#FE5050", "#1DB7E5"], //'#FBFE27','rgb(11,228,96)','#FE5050'
                data: data.sort(function (a, b) {
                    return a.value - b.value;
                }),
                roseType: "radius",

                label: {
                    normal: {
                        formatter: ["{c|{c}}", "{b|{b}}"].join("\n"),
                        rich: {
                            c: {
                                color: "rgb(241,246,104)",
                                fontSize: 12,
                                fontWeight: "bold",
                                lineHeight: 2,
                            },
                            b: {
                                color: "rgb(98,137,169)",
                                fontSize: 12,
                                height: 30,
                            },
                        },
                    },
                },
                labelLine: {
                    normal: {
                        lineStyle: {
                            color: "rgb(98,137,169)",
                        },
                        smooth: 0.2,
                        length: 10,
                        length2: 20,
                    },
                },
                itemStyle: {
                    normal: {
                        shadowColor: "rgba(0, 0, 0, 0.8)",
                        shadowBlur: 50,
                    },
                },
            },
        ],
    };
}