import * as echarts from "echarts";
export const chartOptionFun = (salvProName,salvProValue)=>{
    //   salvProName = [
    //     "川BHZ280",
    //     "川B379KV",
    //     "川B5YM88",
    //     "川BGT710",
    //     "川BGT713",
    //     "川BS5839",
    //     "川B6VC68",
    //     "川B3UP89",
    //     "川BZU101",
    // ];
    //  salvProValue = [239, 181, 154, 144, 135, 117, 74, 72, 67];
    let salvProMax = []; //背景按最大值
    for (let i = 0; i < salvProValue.length; i++) {
        salvProMax.push(salvProValue[0]);
    }
    return  {
        // backgroundColor: "#003366",
        title: {
            x: "50%",
            y: "0",
            textAlign: "center",
            top: "2", //字体的位置
            text: "车辆里程排行",
            textStyle: {
                fontWeight: "normal",
                color: "#FFF",
                fontSize: 18,
            },
        },
        grid: {
            left: "2%",
            right: "2%",
            bottom: "1%",
            top: "16%",
            containLabel: true,
        },
        tooltip: {
            trigger: "axis",
            axisPointer: {
                type: "none",
            },
            formatter: function (params) {
                return params[0].name + " : " + params[0].value+"公里";
            },
        },
        xAxis: {
            show: false,
            type: "value",
        },
        yAxis: [
            {
                type: "category",
                inverse: true,
                axisLabel: {
                    show: true,
                    textStyle: {
                        color: "#fff",
                    },
                },
                splitLine: {
                    show: false,
                },
                axisTick: {
                    show: false,
                },
                axisLine: {
                    show: false,
                },
                data: salvProName,
            },
            {
                type: "category",
                inverse: true,
                axisTick: "none",
                axisLine: "none",
                show: true,
                axisLabel: {
                    textStyle: {
                        color: "#ffffff",
                        fontSize: "12",
                    },
                },
                data: salvProValue,
            },
        ],
        series: [
            {
                name: "值",
                type: "bar",
                zlevel: 1,
                itemStyle: {
                    normal: {
                        barBorderRadius: 30,
                        color: new echarts.graphic.LinearGradient(0, 0, 1, 0, [
                            {
                                offset: 0,
                                color: "rgb(57,89,255,1)",
                            },
                            {
                                offset: 1,
                                color: "rgb(46,200,207,1)",
                            },
                        ]),
                    },
                },
                barWidth: 10,
                data: salvProValue,
            },
            {
                name: "背景",
                type: "bar",
                barWidth: 10,
                barGap: "-100%",
                data: salvProMax,
                itemStyle: {
                    normal: {
                        color: "rgba(24,31,68,1)",
                        barBorderRadius: 30,
                    },
                },
            },
        ],
    };
}